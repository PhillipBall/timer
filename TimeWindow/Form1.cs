﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeWindow
{
    public partial class Form1 : Form
    {
        System.Timers.Timer t;
        int d, h, m, s;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            t = new System.Timers.Timer();
            t.Interval = 1000;
            t.Elapsed += Time3;
        }

        private void Time3(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                s += 1;
                if (s == 60)
                {
                    s = 0;
                    m += 1;
                }
                if (m == 60)
                {
                    m = 0;
                    h += 1;
                }
                if (h == 24)
                {
                    h = 0;
                    d += 1;
                }
                textBox2.Text = string.Format("{0}:{1}:{2}:{3}", d.ToString().PadLeft(2, '0') ,h.ToString().PadLeft(2, '0'), m.ToString().PadLeft(2, '0'), s.ToString().PadLeft(2, '0'));
            }));
        }
        private void button1_Click(object sender, EventArgs e)
        {
            t.Start();
        }

        
    }
}
